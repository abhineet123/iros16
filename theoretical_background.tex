%!TEX ROOT = mtf.tex

\section{Theoretical Background}
\label{background}
\subsection{Notation}
Let $ I_t : \mathbb R^2\mapsto \mathbb R $ refer to an image captured at time $t$ so that a video stream can be modeled as a sequence of images  $\{ I_t | t\geq 0\}$ . The pixel locations in an image patch with $N$ pixels are denoted by a $ 2{\times} N $ matrix $\mathbf{x}=[\mathbf{x_1}, \mathbf{x_2}, ..., \mathbf{x_N}]$ where $\mathbf{x_k}=[x_k, y_k]^T \in  \mathbb R^2$ are the Cartesian coordinates of pixel $ k $ in image space. The corresponding pixel intensities in image $I$ are represented by an $ N{\times} 1 $ vector $\mathbf{I}(\mathbf{x}) = [I(x_1, y_1), I(x_2, y_2), ... , I(x_N, y_N)]^T  \in \mathbb R^N $. Additionally, let $\mathbf{x_0}$ denote the target patch specified in the first image $ I_0$. Note that even though a conventional image is only defined at integral coordinates, sub pixel interpolation \cite{Dame10_mi_thesis} can be used to make $ I_t $ a smooth function of real values. 

Let $\mathbf{w}(\mathbf{x}, \mathbf{p}) : \mathbb{R}^2 \times \mathbb{R}^S\mapsto \mathbb{R}^2$ denote a warping function of $ S $ parameters $\mathbf{p}=(p_1, p_2, ..., p_S)$ that represents the set of allowable image motions of the tracked object by specifying the deformations that can be applied to $\mathbf{x_0}$ to align it with the patch corresponding to the object's location in the current image. Some commonly used examples of $ \mathbf{w} $ include homography, affine, similitude, isometry and translation \cite{Szeliski06_fclk_extended}.
%When applied to an image patch $\mathbf{x}$, the transformed patch is denoted by $\mathbf{w}(\mathbf{x},\mathbf{p})$ and the corresponding pixel values become $\mathbf{I}(\mathbf{w}(\mathbf{x},\mathbf{p}))$.

Finally let $f(\mathbf{I^*}, \mathbf{I^c}) : \mathbb{R}^N \times \mathbb{R}^N \mapsto \mathbb{R}$ be a function that measures the similarity between two sets of pixel values : the reference or template patch $\mathbf{I^*}$, typically extracted from $ I_0 $, and a candidate patch $ \mathbf{I^c} $ extracted from the current image $ I_t $ . The sum of squared differences (SSD) has been the most popular similarity function in literature \cite{Lucas81lucasKanade, Hager98parametricModels, Shum00_fc, Benhimane07_esm_journal} though others like sum of conditional variance (SCV) \cite{Richa11_scv_original}, normalized cross correlation (NCC) \cite{Briechle2001_ncc_template_matching, Scandaroli2012_ncc_tracking}, mutual information (MI) \cite{Dowson08_mi_ict, Dame10_mi_thesis} and cross cumulative residual entropy (CCRE) \cite{Wang2007_ccre_registration, Richa12_robust_similarity_measures} have also been used. 
\subsection{Registration based tracking}
\label{registration_tracking}
With the above notation, registration based tracking can be formulated (Eq \ref{tracking}) as a search problem where the goal is to find the optimal warping parameters $\mathbf{p_t}$ for an image $\mathbf{I_t}$ that maximizes the similarity, measured by $f$, between the target patch 
%$\mathbf{I^*} = \mathbf{I_0}(\mathbf{x_0})$
$\mathbf{I^*} = \mathbf{I_0}(\mathbf{w}(\mathbf{x_0},\mathbf{p_0})) = \mathbf{I_0}(\mathbf{x_0})$
and the warped image patch $\mathbf{I^c}=\mathbf{I_t}(\mathbf{w}(\mathbf{x},\mathbf{p_t}))$. 
\begin{equation}
\begin{aligned}
\label{tracking}
\mathbf{p_t} = \underset{\mathbf{p}} {\mathrm{argmax}} ~f(\mathbf{I_0}(\mathbf{x_0}),\mathbf{I_t}(\mathbf{w}(\mathbf{x_0},\mathbf{p})))
\end{aligned}
\end{equation}
%Here $ \mathbf{p_0} $ usually corresponds to the identity element of $ \mathbf{w} $ so that $ \mathbf{w}(\mathbf{x},\mathbf{p_0}) = \mathbf{x}$.

As has been observed before \cite{Szeliski06_fclk_extended, Richa12_robust_similarity_measures}, this formulation gives rise to an intuitive way to decompose the tracking task into three modules - the similarity metric $ f $, the warping function $ \mathbf{w} $ and  the optimization approach. These can be designed to be  semi independent in the sense that any given optimizer can be applied unchanged to several combinations of methods for the other two modules which in tun interact only through a well defined and consistent interface. 
%This decomposition is an integral part of MTF and is described in more detail in Sec. \ref{}. 
In this work, we refer to these modules respectively as AM, SSM and SM.

\subsection{Gradient Descent and the Chain Rule}
%Though several approaches have been reported in literature for the SM, including nearest neighbor search \cite{Dick13nn}, linear regression, difference decomposition \cite{Gleicher97_diff_decomp} and sequential Monte Carlo search \cite{Isard98condensation}, methods based on gradient descent optimization have remained the most popular since they were introduced in the classical Lucas Kanade (LK) tracker\cite{Lucas81lucasKanade} due to their simplicity and speed. 
Though several types of SMs have been reported in literature, gradient descent based approaches \cite{Lucas81lucasKanade} have been most widely used due to their speed and simplicity.
As mentioned in \cite{Baker04lucasKanade_paper}, the LK tracker can be formulated in four different ways depending on which image is searched for the warped patch - $ I_0 $ or $ I_t $ - and how the parameters of the warping function are updated in each iteration - additive or compositional. The four resultant formulations are thus called Forward Additive (\textbf{FALK}) \cite{Lucas81lucasKanade}, Inverse Additive (\textbf{IALK})\cite{Hager98parametricModels}, Forward Compositional (\textbf{FCLK}) \cite{Shum00_fc} and Inverse Compositional (\textbf{ICLK}) \cite{Baker01ict}. In addition, a relatively recent approach is the Efficient Second order Minimization (\textbf{ESM})\cite{Benhimane07_esm_journal} technique that tries to make the best of both inverse and forward formulations by using information from both $ I_0 $ or $ I_t $.

What all these methods have in common is that they solve Eq \ref{tracking} by estimating an incremental update $ \Delta\mathbf{p}_t $ to the optimal SSM parameters $  \mathbf{p}_{t-1} $ at time $ t-1 $ using some variant of the Newton method as:
\begin{equation}
\label{eq_nwt_method}
\Delta\mathbf{p}_t=-\hat{\mathbf{H}}^{-1}\hat{\mathbf{J}}^T
\end{equation}
where  $ \hat{\mathbf{J}} $ and $ \hat{\mathbf{H}} $ respectively are estimates for the $ 1{\times} S $ Jacobian  $  \mathbf{J} = \dfrac{\partial f}{\partial \mathbf{p}}$ and the $ S{\times} S $ Hessian $  \mathbf{H} = \dfrac{\partial^2 f}{\partial \mathbf{p}^2}$ of the AM w.r.t. SSM parameters. 
For any formulation that seeks to decompose this class of trackers (among others) in the manner mentioned in Sec. \ref{registration_tracking}, the chain rule for first and second order derivatives is indispensable and
%relevant notation will be introduced next to make the subsequent system description easier.
the resultant decompositions for $  \mathbf{J} $ and $  \mathbf{H} $ are given by Eqs. \ref{eq_jac_basic} and \ref{eq_hess_basic} respectively.
\begin{equation}
\label{eq_jac_basic}
 \mathbf{J} = 
 \dfrac{\partial f(
 \mathbf{I}(
 \mathbf{w}(\mathbf{p}) 
 )
 )}
 {\partial \mathbf{p}} = 
\dfrac{\partial f}{\partial \mathbf{I}} 
\nabla \mathbf{I}
\dfrac{\partial  \mathbf{w}}{\partial \mathbf{p}}
\end{equation}

%where $  \dfrac{\partial f}{\partial \mathbf{I}}  $ is the $ 1{\times} N $ Jacobian of AM w.r.t. image pixel values, 
%$ \dfrac{\partial \mathbf{I}}{\partial \mathbf{w}}$ is the $ N{\times} 2 $ spatial gradient of the image and  
%$ \dfrac{\partial  \mathbf{w}}{\partial \mathbf{p}}$ is the $ 2{\times} S{\times}N $ Jacobian of the warped pixel locations w.r.t. SSM parameters. 
%Note that the multiplication between the last two terms in Eq. \ref{eq_jac_basic} is carried out on a per pixel basis by multiplying each row of $ \dfrac{\partial \mathbf{I}}{\partial \mathbf{w}}$  with the corresponding  $ 2{\times} S $ block of the tensor $ \dfrac{\partial  \mathbf{w}}{\partial \mathbf{p}}$ and stacking the results into the rows of an  $ N{\times} S $ matrix $  \dfrac{\partial \mathbf{I}}{\partial \mathbf{p}} $. This detail is assumed implicit in the notation for brevity.
%The generic decomposition for the Hessian is given as:
\begin{align}
\label{eq_hess_basic}
\mathbf{H}
=
\dfrac{\partial \mathbf{I}}{\partial \mathbf{p}}^T
\dfrac{\partial^2 f}{\partial \mathbf{I}^2}
\dfrac{\partial \mathbf{I}}{\partial \mathbf{p}}
+
\dfrac{\partial f}{\partial \mathbf{I}}
\dfrac{\partial^2 \mathbf{I}}{\partial \mathbf{p}^2}
\end{align} 
with
$
\dfrac{\partial \mathbf{I}}{\partial \mathbf{p}}=\nabla \mathbf{I}
\dfrac{\partial\mathbf{w}}{\partial \mathbf{p}}
$
and
$
\dfrac{\partial^2 \mathbf{I}}{\partial \mathbf{p}^2}=\dfrac{\partial\mathbf{w}}{\partial \mathbf{p}}^T
\nabla^2\mathbf{I}
\dfrac{\partial \mathbf{w}}{\partial \mathbf{p}}
+
\nabla\mathbf{I}
\dfrac{\partial^2 \mathbf{w}}{\partial \mathbf{p}^2}
$

These generic expressions, however, do not give the whole scope of the decompositions since $ f $ is a function of \textit{two} sets of pixel values so that $ \mathbf{J} $ and $ \mathbf{H} $ depend on which of the two corresponding images is being searched for the optimal warp as only this one varies with the SSM parameters. In addition, due to the way the Taylor series expansion is formulated, the exact forms of the image derivatives $\nabla\mathbf{I} $ and $ \nabla^2\mathbf{I} $ too depend on whether additive or compositional updates are to be used. The reader is referred to \cite{Baker04lucasKanade_paper} for more details. The formulations relevant to the functions specified in Tables \ref{tab_am_func_specification} and \ref{tab_ssm_func_specification}, including some extensions to \cite{Baker04lucasKanade_paper}, are presented in the \hyperref[appendix]{appendix}.
%\input{jacobian}
%\input{hessian}

\subsection{Stochastic Search}
An important limitation of LK type trackers, in common with non linear optimization methods,  is that they are prone to getting stuck in local maxima of $ f $ especially when the object's appearance changes due to factors like motion blur, illumination variations and occlusions. An alternative approach to avoid this problem is to use stochastic search so as to cover a larger portion of the SSM search space. There are two main categories of such methods in our framework currently - sequential Monte Carlo search or particle filters (\textbf{PF}) \cite{Isard98condensation, Kwon2014_sl3_aff_pf} and approximate nearest neighbor search (\textbf{NN}) \cite{Dick13nn}. 

These methods work by generating a set of random samples for the SSM parameters $ \mathbf{p} $ and evaluating the goodness of each sample by some measure of similarity with the template. Then they either select one of the samples (NN) or perform some kind of weighted averaging over the samples (PF) to get an estimate of the object's location. 
%It maybe noted that, in analogy with the previous section, NN corresponds to the inverse formulation as it generates its samples around $ \mathbf{I_0}(\mathbf{x_0}) $ while PF generates new samples in each frame around $ \mathbf{I_t}( \mathbf{\hat{p}_t}) $ and is thus equivalent to the forward formulation.

As a result, their performance depends mostly on the number and quality of stochastic samples used. While the former is limited only by the available computational resources, the latter is a bit harder to guarantee for a general SSM. For methods that draw samples from a Gaussian distribution, the quality thereof is determined by the covariance matrix used and, to the best of our knowledge, no widely accepted method of estimating it for a general SSM exists. Most published works either use heuristics or perform extensive hand tuning to get acceptable results, sometimes even using different values for each tested sequence \cite{Kwon2014_sl3_aff_pf}. 

Given this, a reasonable way to decompose these methods to fit our framework is to delegate the responsibility of generating the set of samples and estimating its mean entirely to the SSM and let the AM evaluate the suitability of each sample by providing the likelihood of the corresponding patch. Since the definition of what constitutes a good sample and how the mean of a sample set is to be evaluated depends on the SSM itself, as do any heuristics for generating these samples
(like the variance for each component of $ \mathbf{p} $), such a decomposition ensures both theoretical validity and good performance in practice. 

%In analogy with the last section, NN trackers can be regarded as equivalent to the inverse formulation since they generate samples from $ I_0 $ owing to the impracticality of rebuilding the index in real time.  . Though this makes them more robust to changes in the object's appearance, it also constrains both the number of samples and the dimensionality the SSM that can be used while gettng real time performance. This is the reason why PF based trackers have mostly been limited to low DOF SSM \cite{Isard98condensation, Khan04_particle_filter_main} though some high DOF versions have been introduced lately \cite{Kwon2009_aff_pf, Kwon2014_sl3_aff_pf} too.