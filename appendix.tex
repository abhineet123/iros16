%!TEX ROOT = modularDecomposition.tex
%\clearpage
%\appendix
\begin{appendices}
%\section*{Appendix: Jacobian and Hessian formulations for different variants of LK}
\section*{Appendix: $ \hat{\mathbf{J}} $ and $ \hat{\mathbf{H}} $ for different variants of LK}
\label{appendix}
\subsection{Jacobian}
\label{jacobian}
Denoting $\mathbf{w}(\mathbf{x_0},\mathbf{p})$ with $ \mathbf{w}(\mathbf{p}) $ for conciseness (and because $ \mathbf{x_0} $ is constant in this context) and letting  $\mathbf{\hat{p}_t}$ denote an estimate of $\mathbf{p_t}$ to which an incremental update is sought, the formulations for $ \hat{\mathbf{J}} $ used by FALK and FCLK are:
\begin{equation}
\label{eq_jac_falk}
 \hat{\mathbf{J}}_{fa} = 
 \left.\dfrac{\partial f}{\partial \mathbf{I^c}}\right\rvert_{\mathbf{I^c}=\mathbf{I_t}(\mathbf{w}(\mathbf{\hat{p}_t}))}
 \left.
  	 	\nabla\mathbf{I_t}
 \right\rvert_{\mathbf{x}=\mathbf{w}(\mathbf{\hat{p}_t})}  
 \left.
 	\dfrac{\partial \mathbf{w}}{\partial \mathbf{p}}
 \right\rvert_{\mathbf{p}=\mathbf{\hat{p}_t}}
\end{equation}
\begin{align}
\label{eq_jac_fclk}
 \hat{\mathbf{J}}_{fc} = 
 \left.\dfrac{\partial f}{\partial \mathbf{I^c}}\right\rvert_{\mathbf{I^c}=\mathbf{I_t}(\mathbf{w}(\mathbf{\hat{p}_t}))}
\left.
\nabla \mathbf{I_t} (\mathbf{w})
\right\rvert_{\mathbf{x}=\mathbf{x_0}}  
\left.
   \dfrac{\partial \mathbf{w}}{\partial \mathbf{p}}
\right\rvert_{\mathbf{p}=\mathbf{p_0}}
\end{align}
where $ \nabla \mathbf{I_t} (\mathbf{w}) $ in Eq. \ref{eq_jac_fclk} refers to the gradient of $ I_t $ warped using $\mathbf{\hat{p}_t}$,
i.e. $ I_t $ is first warped back to the coordinate frame of $ I_0 $ using $ \mathbf{w}(\mathbf{\hat{p}_t})$ to obtain $ I_t(\mathbf{w}) $ whose gradient is then computed at $ \mathbf{x}=\mathbf{x_0} $.
It can be further expanded \cite{Baker04lucasKanade_paper} as:
\begin{equation}
\label{eq_jac_warped_grad}
\left.
	\nabla \mathbf{I_t} (\mathbf{w})
\right\rvert_{\mathbf{x}=\mathbf{x_0}}  
=
 \left.
  	 	\nabla \mathbf{I_t}
 \right\rvert_{\mathbf{x}=\mathbf{w}(\mathbf{\hat{p}_t})} 
\left.
	\dfrac{\partial \mathbf{w}}{\partial \mathbf{x}}
\right\rvert_{\mathbf{p}=\mathbf{\hat{p}_t}}
\end{equation}
%where $ \dfrac{\partial \mathbf{I_t}}{\partial \mathbf{w}} $ is the gradient of $ I_t $ evaluated at $ \mathbf{x}=\mathbf{w}(\mathbf{\hat{p}_t})$.
Since $ \nabla \mathbf{I_t} $ is usually the most computationally intensive part of $  \mathbf{J}_{fc} $ and $  \mathbf{J}_{fa} $, the so called inverse methods approximate this with the gradient of $ \nabla \mathbf{I_0} $ for efficiency as this only needs to be computed once. The specific expressions for these methods are:
\begin{equation}
\label{eq_jac_iclk}
 \hat{\mathbf{J}}_{ic} =
 \left.
 	\dfrac{\partial f}{\partial \mathbf{I^*}}
 \right\rvert_{\mathbf{I^*}=\mathbf{I_0}(\mathbf{x_0})}
 \left.
 	\nabla \mathbf{I_0}
 \right\rvert_{\mathbf{x}=\mathbf{x_0}}  
 \left.
 	\dfrac{\partial \mathbf{w}}{\partial \mathbf{p}}
 \right\rvert_{\mathbf{p}=\mathbf{p_0}}
\end{equation}
\begin{equation}
\label{eq_jac_ialk}
 \hat{\mathbf{J}}_{ia} = 
 \left.\dfrac{\partial f}{\partial \mathbf{I^c}}\right\rvert_{\mathbf{I^c}=\mathbf{I_t}(\mathbf{w}(\mathbf{\hat{p}_t}))}
 \left.\nabla \mathbf{I_0}\right\rvert_{\mathbf{x}=\mathbf{x_0}}
 \left.\dfrac{\partial \mathbf{w}}{\partial \mathbf{x}}^{-1}\right\rvert_{\mathbf{p}=\mathbf{\hat{p}_t}}  
 \left.\dfrac{\partial \mathbf{w}}{\partial \mathbf{p}}\right\rvert_{\mathbf{p}=\mathbf{\hat{p}_t}}
\end{equation}
where the middle two terms in Eq. \ref{eq_jac_ialk} are derived from Eqs. \ref{eq_jac_falk} and \ref{eq_jac_warped_grad} by assuming \cite{Hager98parametricModels} that $\mathbf{w}(\mathbf{\hat{p}_t})$ perfectly aligns $ I_t $ with $ I_0 $, i.e.
$ I_t(\mathbf{w}) = I_0 $ so that
\begin{align}
\label{eq_ialk_assumption}
%\mathbf{I_t}(\mathbf{w}(\mathbf{\hat{p}_t})) = \mathbf{I_t^w}(\mathbf{x_0}) = \mathbf{I_0}(\mathbf{x_0})
\nabla \mathbf{I_t} (\mathbf{w})=\nabla \mathbf{I_0}
\end{align}
%which, combined with Eq. \ref{eq_jac_warped_grad}, gives the middle two terms of  Eq. \ref{eq_jac_ialk}.
In its original paper \cite{Benhimane07_esm_journal}, ESM was formulated as using the mean of the pixel gradients $\nabla \mathbf{I_0}$ and $\nabla \mathbf{I_t}(\mathbf{w})$ to compute $ \mathbf{J} $ but, as this formulation is only applicable to SSD, we consider a generalized version \cite{Brooks10_esm_ic, Scandaroli2012_ncc_tracking} that uses the \textit{difference} between FCLK and ICLK Jacobians:
\begin{align}
\label{eq_jac_esm}
 \hat{\mathbf{J}}_{esm} =  \hat{\mathbf{J}}_{fc} -   \hat{\mathbf{J}}_{ic}
\end{align}

\subsection{Hessian}
\label{hessian}
%where $ \dfrac{\partial^2 f}{\partial \mathbf{I}^2} $ is the $ N{\times} N $ Hessian of AM w.r.t. pixel values while $ \dfrac{\partial^2 \mathbf{I}}{\partial \mathbf{p}^2} $ is the $ S{\times} S $ Hessian of pixel values w.r.t. SSM parameters and can be further split as:
%where $ \dfrac{\partial^2 \mathbf{I}}{\partial \mathbf{p}^2} $ can be further split as:
%\begin{align}
%\label{eq_pix_hess_basic}
%\dfrac{\partial^2 \mathbf{I}}{\partial \mathbf{p}^2}
%=
%\dfrac{\partial\mathbf{w}}{\partial \mathbf{p}}^T
%\dfrac{\partial^2 \mathbf{I}}{\partial \mathbf{w}^2}
%\dfrac{\partial \mathbf{w}}{\partial \mathbf{p}}
%+
%\dfrac{\partial\mathbf{I}}{\partial \mathbf{w}}
%\dfrac{\partial^2 \mathbf{w}}{\partial \mathbf{p}^2}
%\end{align} 
%where $ \dfrac{\partial^2 \mathbf{I}}{\partial \mathbf{w}^2} $ is the  $ 2{\times} 2{\times}N $ image Hessian while $ \dfrac{\partial^2 \mathbf{w}}{\partial \mathbf{p}^2} $ is $ S{\times} S{\times} 2 $ SSM Hessian where, as in Eq. \ref{eq_jac_basic}, these products are computed pixel wise and the tensor products amount to plane-wise operations as:
%\begin{align}
%\dfrac{\partial\mathbf{I}}{\partial \mathbf{w}}
%\dfrac{\partial^2 \mathbf{w}}{\partial \mathbf{p}^2}
%='''

%\dfrac{\partial\mathbf{I}}{\partial \mathbf{w_x}}
%\dfrac{\partial^2 \mathbf{w_x}}{\partial \mathbf{p}^2}
%+
%\dfrac{\partial\mathbf{I}}{\partial \mathbf{w_y}}
%\dfrac{\partial^2 \mathbf{w_y}}{\partial \mathbf{p}^2}
%\end{align}
For clarity and brevity, evaluation points for the various terms have been omitted in the equations that follow as being obvious from analogy with the previous section.

It is generally assumed \cite{Baker04lucasKanade_paper, Benhimane07_esm_journal} that the second term of Eq. \ref{eq_hess_basic} is too costly to compute and too small near convergence to matter and so is omitted to give the so called Gauss Newton Hessian
\begin{align}
\label{eq_hess_basic_gn}
\mathbf{H}_{gn}
=
\dfrac{\partial \mathbf{I}}{\partial \mathbf{p}}^T
\dfrac{\partial^2 f}{\partial \mathbf{I}^2}
\dfrac{\partial \mathbf{I}}{\partial \mathbf{p}}
\end{align}
Though $ \mathbf{H}_{gn} $ works very well for SSD (and in fact even better than $ \mathbf{H}$ \cite{Baker04lucasKanade_paper,Dame10_mi_thesis}), it is well known \cite{Dame10_mi_thesis, Dame10_mi_thesis, Scandaroli2012_ncc_tracking} to \textit{not} work well with other AMs like MI, CCRE and NCC for which an approximation to the Hessian \textit{after convergence} has to be used by assuming perfect alignment or $ \mathbf{I_t}(\mathbf{w}(\mathbf{\hat{p}_t})) = \mathbf{I_0}(\mathbf{x_0})$. We refer to the resultant approximation as the \textbf{Self Hessian} $ \mathbf{H}_{self} $ and, as this substitution can be made by setting either $ \mathbf{I^c} = \mathbf{I_0}(\mathbf{x_0}) $ or $ \mathbf{I^*} = \mathbf{I_t}(\mathbf{w}(\mathbf{\hat{p}_t})) $, we get two forms which are respectively deemed to be the Hessians for ICLK and FCLK:
\begin{align}
\label{eq_hess_iclk}
\hat{\mathbf{H}}_{ic}
=
\mathbf{H}_{self}^\mathbf{*}
=
\dfrac{\partial \mathbf{\mathbf{I_0}}}{\partial \mathbf{p}}^T
\dfrac{\partial^2 f(\mathbf{I_0}, \mathbf{I_0})}{\partial \mathbf{I}^2}
\dfrac{\partial \mathbf{\mathbf{I_0}}}{\partial \mathbf{p}}
+
\dfrac{\partial f(\mathbf{I_0}, \mathbf{I_0})}{\partial \mathbf{I}}
\dfrac{\partial^2 \mathbf{\mathbf{I_0}}}{\partial \mathbf{p}^2}
\end{align}
\begin{align}
\label{eq_hess_fclk}
\hat{\mathbf{H}}_{fc}
=
\mathbf{H}_{self}^\mathbf{c}
=
\dfrac{\partial \mathbf{\mathbf{I_t}}}{\partial \mathbf{p}}^T
\dfrac{\partial^2 f(\mathbf{I_t}, \mathbf{I_t})}{\partial \mathbf{I}^2}
\dfrac{\partial \mathbf{\mathbf{I_t}}}{\partial \mathbf{p}}
+
\dfrac{\partial f(\mathbf{I_t}, \mathbf{I_t})}{\partial \mathbf{I}}
\dfrac{\partial^2 \mathbf{\mathbf{I_t}}}{\partial \mathbf{p}^2}
\end{align}
It is interesting to note that $ \mathbf{H}_{gn} $ has the exact same form as $ \mathbf{H}_{self} $ for SSD (since $ \dfrac{\partial f_{ssd}(\mathbf{I_0}, \mathbf{I_0})}{\partial \mathbf{I}} =  \dfrac{\partial f_{ssd}(\mathbf{I_t}, \mathbf{I_t})}{\partial \mathbf{I}} = \mathbf{0}$) so it seems that interpreting Eq. \ref{eq_hess_basic_gn} as the first order approximation of Eq. \ref{eq_hess_basic}, as in \cite{Baker04lucasKanade_paper, Dowson08_mi_ict, Dame10_mi_thesis}, is incorrect and it should instead be seen as a special case of $ \mathbf{H}_{self} $.

$ \hat{\mathbf{H}}_{fa} $ differs from $ \hat{\mathbf{H}}_{fc} $ only in the way $ \dfrac{\partial^2 \mathbf{\mathbf{I_t}}}{\partial \mathbf{p}^2} $ and $ \dfrac{\partial \mathbf{\mathbf{I_t}}}{\partial \mathbf{p}} $ are computed for the two as given in Eqs. \ref{eq_pix_hess_falk} and \ref{eq_pix_hess_fclk} respectively.
%As in the case of the Jacobians, the Hessians for FALK and FCLK have the same form (Eq. \ref{eq_hess_fclk}), differing only in the way the pixel Hessian $ \dfrac{\partial^2 \mathbf{\mathbf{I_t}}}{\partial \mathbf{p}^2} $ and Jacobian $ \dfrac{\partial \mathbf{\mathbf{I_t}}}{\partial \mathbf{p}} $ are computed. 
\begin{align}
\label{eq_pix_hess_falk}
\dfrac{\partial^2 \mathbf{\mathbf{I_t}}}{\partial \mathbf{p}^2} (fa)
= 
\dfrac{\partial\mathbf{w}}{\partial \mathbf{p}}^T
\nabla^2 \mathbf{I_t}
\dfrac{\partial \mathbf{w}}{\partial \mathbf{p}}
+
\nabla\mathbf{I_t}
\dfrac{\partial^2 \mathbf{w}}{\partial \mathbf{p}^2}
\end{align}
\begin{align}
\label{eq_pix_hess_fclk}
\dfrac{\partial^2 \mathbf{\mathbf{I_t}}}{\partial \mathbf{p}^2} (fc)
= 
\dfrac{\partial\mathbf{w}}{\partial \mathbf{p}}^T
\nabla^2 \mathbf{I_t}(\mathbf{w})
\dfrac{\partial \mathbf{w}}{\partial \mathbf{p}}
+
\nabla\mathbf{I_t}(\mathbf{w})
\dfrac{\partial^2 \mathbf{w}}{\partial \mathbf{p}^2}
\end{align}
%where $ \dfrac{\partial^2 \mathbf{w}}{\partial \mathbf{x}^2} $ is the $ 2{\times} 2{\times} 2 $ Hessian of SSM w.r.t. pixel coordinates and , as in Eq. \ref{eq_pix_hess_basic}, the tensor product is computed plane wise.
where $\nabla^2 \mathbf{I_t}(\mathbf{w})$ can be expanded by differentiating Eq. \ref{eq_jac_warped_grad} as:
\begin{align}
\label{eq_warped_pix_hess}
\nabla^2 \mathbf{I_t}(\mathbf{w})
=
\dfrac{\partial \mathbf{w}}{\partial \mathbf{x}}^T
\nabla^2 \mathbf{I_t}
\dfrac{\partial \mathbf{w}}{\partial \mathbf{x}}
+
\nabla\mathbf{I_t}
\dfrac{\partial^2 \mathbf{w}}{\partial \mathbf{x}^2}
\end{align}
$ \hat{\mathbf{H}}_{ia} $ is identical to $ \hat{\mathbf{H}}_{fa} $ except that $ \nabla\mathbf{I_0} $ and $ \nabla^2\mathbf{I_0} $ are used to approximate $ \nabla\mathbf{I_t} $ and $ \nabla^2\mathbf{I_t}$. The expression for the former is in Eq. \ref{eq_jac_ialk} while that for the latter can be derived by differentiating both sides of Eq. \ref{eq_jac_warped_grad} after substituting Eq. \ref{eq_ialk_assumption}:
\begin{align*}
\nabla^2 \mathbf{I_0}
=
\dfrac{\partial \mathbf{w}}{\partial \mathbf{x}}^T
\nabla^2 \mathbf{I_t}
\dfrac{\partial \mathbf{w}}{\partial \mathbf{x}}
+
\nabla\mathbf{I_t}
\dfrac{\partial^2 \mathbf{w}}{\partial \mathbf{x}^2}
\end{align*}
which gives:
\begin{align}
\label{eq_ialk_pix_hess}
\nabla^2 \mathbf{I_t}(ia)
&=
\left(\dfrac{\partial \mathbf{w}}{\partial \mathbf{x}}^{-1}\right)^T
\left[
\nabla^2 \mathbf{I_0}
-
\nabla\mathbf{I_t}
\dfrac{\partial^2 \mathbf{w}}{\partial \mathbf{x}^2}
\right]
\dfrac{\partial \mathbf{w}}{\partial \mathbf{x}}^{-1}
\nonumber\\
&=
\left(\dfrac{\partial \mathbf{w}}{\partial \mathbf{x}}^{-1}\right)^T
\left[
\nabla^2 \mathbf{I_0}
-
\left(
\nabla\mathbf{I_0}
\dfrac{\partial \mathbf{w}}{\partial \mathbf{x}}^{-1}
\right)
\dfrac{\partial^2 \mathbf{w}}{\partial \mathbf{x}^2}
\right]
\dfrac{\partial \mathbf{w}}{\partial \mathbf{x}}^{-1}
\end{align}
where the second equality again follows from Eq. \ref{eq_jac_warped_grad} and \ref{eq_ialk_assumption}.
Finally, the ESM Hessian corresponding to the Jacobian in Eq. \ref{eq_jac_esm} is the \textit{sum} of FCLK and ICLK Hessians:
\begin{align}
\label{eq_hess_esm}
\hat{\mathbf{H}}_{esm} = \hat{\mathbf{H}}_{fc} +  \hat{\mathbf{H}}_{ic}
\end{align}
\end{appendices}

