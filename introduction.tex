%!TEX ROOT = mtf.tex

\section{Introduction}
\label{introduction}

Fast and high precision visual tracking is crucial to the success of several robotics and virtual reality applications like SLAM, autonomous navigation and visual servoing. In recent years, online learning and detection based trackers have been more popular in the vision community \cite{Wu13benchmark, Kristan2015_vot15} due to their robustness to changes in the object's appearance
which makes them better suited to long term tracking.
%However, these are often too slow and imprecise to be useful for the aforementioned applications which, as a result, employ registration based trackers instead.
However, these are often unsuitable for the aforementioned applications for two reasons. Firstly, they are too slow to allow real time execution of tasks where multiple trackers have to be run simultaneously or tracking is only a small part of a larger system with more computationally intensive modules that use its result to make higher level deductions about the environment. Secondly, they are not precise enough to give the exact object pose with sub pixel alignment required for these tasks, being usually limited to the estimation of simple transformations of the target patch such as translation and scaling.
As a result, registration based trackers are more suitable for these applications as being several times faster and capable of estimating more complex transformations like affine and homography.

Though several major improvements have been made in this domain since the original Lucas Kanade (LK) tracker was introduced almost thirty five years ago \cite{Lucas81lucasKanade}, yet efficient open source implementations of recent trackers are surprisingly difficult to find. In fact the only such tracker offered by the popular OpenCV library \cite{opencv_library}, uses a pyramidal implementation of the original algorithm \cite{Bouguet2002Pyramidal}. Similarly the ROS library \cite{Quigley09_ros}, widely used by the robotics community, currently does not have any package that implements a modern registration based tracker. The XVision system \cite{hager1998_xvision} did introduce a full tracking framework including an efficient video pipeline. However, it only implemented several variants of the same algorithm \cite{Hager98parametricModels} that only gave reasonable tracking performance with low degrees of freedom (DOF) motion. In addition, it is not well documented and is quite difficult to install on modern systems due to many obsolete dependencies.  Even the fairly modern \href{http://www.mrpt.org/}{MRPT}  library \cite{Harris11_robotics_sw_survey} includes only a version of the original LK tracker, much like OpenCV, apart from a low DOF particle filter based tracker which is too imprecise and slow to be considered relevant for our target applications.

In the absence of good open source implementations of modern trackers, most robotics and VR research groups either use these out dated trackers or implement their own custom trackers. These, in turn, are often not made publicly available or are tailored to suit very specific needs and so require significant reprogramming to be useful for an unrelated project.
To address this requirement, we introduce Modular Tracking Framework (\textbf{MTF}) \footnote{available online at \url{https://bitbucket.org/abhineet123/mtf}} - a generic system for registration based tracking that provides highly efficient implementations for a large subset of trackers introduced in literature to date and is designed to be easily extensible with additional methods. 

At the same time, we also present a new way to study registration based trackers by decomposing them into three constituent sub modules: Appearance Model (\textbf{AM}), State Space Model (\textbf{SSM}) and Search Method (\textbf{SM}). This is needed to address another urgent need in this field - to unify the myriad of contributions made in the last three decades so they can be better understood.
It is often the case that when a new tracker is introduced in literature, it only contributes to one or two of these sub modules while using existing methods for the rest. Since these are often selected arbitrarily by the authors, they may not be optimal for the new method. In such cases, this breakdown can help to experimentally find the best combination of methods for these sub modules while also providing a model within which the contributions of any new tracker can be clearly demarcated and thus studied better. 

Finally, our framework has been designed to closely follow this decomposition through extensive use of generic programming so it provides a convenient interface to plug in a new method for any sub module and test it against all possible combinations of methods for the other two sub modules. This will not only help to compare the new method against existing ones in a more comprehensive way but also make it immediately available to any project that uses MTF.

An existing system that is quite similar to MTF in functionality is the template tracker module of the \href{http://visp-doc.inria.fr/doxygen/visp-daily/index.html}{Visual Servoing Platform (ViSP)} library \cite{Marchand05_visp}. The latest version provides 4 SMs, 3 AMs and 6 SSMs though not all combinations work.
%and, not having generic implementations, are difficult to extend.
MTF offers several advantages over ViSP. Firstly, SMs and AMs in ViSP are not implemented as independent modules, rather each combination of methods has its own class. This makes it difficult to add a new method for either of these sub modules and combine it with existing methods for the other. Secondly, 
%ViSP does not perform sub pixel interpolation so is inherently less accurate than MTF but even so
MTF is significantly faster than ViSP - both inverse and forward compositional trackers with SSD/Homography are nearly 30 times faster (2400 vs 87 fps and 615 vs 22 fps respectively on an Intel Core i5 system). Thirdly,  MTF has several more AMs than ViSP as well as two stochastic SMs - NN \cite{Dick13nn} and Particle Filter \cite{Kwon2014_sl3_aff_pf}. Lastly, ViSP trackers seem buggy and subject to random crashes - in our tests they managed to complete only 88 out of 205 sequences.
%Moreover, these are designed only to be utilized within the larger visual servoing functions of this library so are difficult to use with an unrelated project or task.
%Following are the main advantages of MTF over ViSP template tracker:
%\begin{itemize}
%\item SMs and AMs in ViSP are \textit{not} implemented as independent modules, rather each combination of methods has its own class. This makes it difficult to add a new method for either of these sub modules and run it with existing methods for the other.
%%\item ZNCC is only implemented for two search methods
%%\item Member functions in these classes are not well documented so it is difficult to figure out what they do without examining the code making the integration of new methods even more difficult.
%\item MTF has several more AMs than ViSP as well as two stochastic SMs - NN \cite{Dick13nn} and Particle Filter \cite{Kwon2014_sl3_aff_pf}. 
%%\item ViSP tracks the object through two \textit{triangles} that are not constrained to be spatially tied up so getting a precise warped bounding box is not easy if at all possible.
%\item ViSP does not perform sub pixel interpolation so is inherently less accurate than MTF
%\item MTF is \textit{much} faster than ViSP - ICLK is roughly 30 times faster (2400 vs 87 fps on an Intel Core i5 system)
%\item ViSP trackers seem buggy and subject to random crashes - in our tests they managed to complete only 88 out of 205 sequences.
%\end{itemize}

To summarize, following are the main contributions of this work:
\begin{itemize}
\item Provide a unifying formulation for the different variants of LK tracker which can be seen as an extension of the framework reported in \cite{Baker04lucasKanade_paper} using newer AMs and SMs.
\item Present an open source tracking framework which follows this formulation closely to prove its practical validity and, owing to its efficient C++ implementation, can also be used to address practical tracking requirements.
\end{itemize}
Rest of this paper is organized as follows: Section \ref{background} introduces the mathematical basis on which MTF has been designed, section \ref{system_design} describes the class structure of MTF along with specifications of important functions and section \ref{examples} presents several SMs as examples of using the functionality described in section \ref{system_design} to implement the theory of section \ref{background}.





