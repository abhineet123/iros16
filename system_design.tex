%!TEX ROOT = mtf.tex

\section{System Design}
\label{system_design}
As shown in the class diagram in Fig. \ref{fig_class_diagram}, MTF closely follows the decomposition described in the previous sections and has three abstract base classes corresponding to the three sub modules - \texttt{SearchMethod}, \texttt{AppearanceModel} and \texttt{StateSpaceModel}. \footnote{For brevity, these will be referred to as \texttt{SM}, \texttt{AM} and \texttt{SSM} respectively with the font serving to distinguish the \textit{classes} from the corresponding \textit{concepts}.} 
Of these, only  \texttt{SM} is a generic class that is templated on specializations of the other two classes. A concrete tracker, defined as a particular combination of the three sub modules, thus corresponds to a subclass of \texttt{SM} that has been instantiated with subclasses of \texttt{AM} and \texttt{SSM}. 

It may be noted that \texttt{SM} itself derives from a non generic base class called \texttt{TrackerBase} for convenient creation and interfacing of objects corresponding to heterogeneous trackers, including those external to MTF. so that they can be run simultaneously and their results combined to create a tracker more robust than any of its components. Allowing a diverse variety of trackers to integrate seamlessly is one of the core design objectives of MTF and Fig. \ref{fig_class_diagram} emphasizes this by grouping such trackers under a separate category (\texttt{Composite}). Since individual registration based trackers are well known to be prone to failures and more than three decades of research has failed to make significant improvements in this regard, the composite approach seems to be one of the more promising ones  \cite{Zhang2015_rklt} and MTF has been designed specifically to facilitate work in this direction.
\begin{figure*}
\begin{center}
\includegraphics[width=7in]{class_diagram.png}\\
\caption{MTF Class Diagram showing all models currently implemented. Pure and partially abstract classes are respectively shown in red and green while concrete classes are in black. Classes that are sub parts of \texttt{AM} and \texttt{SSM} are in yellow. The node called Composite is not an actual class but represents a conceptual affinity between its derived classes in that they combine outputs from multiple trackers to form a more robust tracker. Acronyms not defined in text: NSSD: Normalized SSD, RSCV: Reversed SCV, LSCV: Localized SCV, LRSCV: Localized RSCV, SSIM: Structural Similarity, SPSS: Sum of Pixel wise SSIM, LKLD: Localized KL Divergence, AESM: Additive ESM, RKLT: \cite{Zhang2015_rklt}}
\label{fig_class_diagram}
\end{center}
\end{figure*}

Since an SM is defined only by its objective - to find the SSM parameters that maximize the similarity measure defined by the AM - particular implementations of \texttt{SM} can cover a potentially wide range of methods that have little in common. As a result, \texttt{SM} is the least specific of these interfaces and only provides functions to initialize, update and reset the tracker along with accessors to obtain its current state. In fact, an SM is regarded in this framework simply as one way to \textit{use} the methods provided by the other two sub modules to accomplish the above objective with the idea being to abstract out as much computation from the SM to the AM/SSM as possible so as to make for a general purpose tracker. 
Therefore, this section describes only \texttt{AM} and \texttt{SSM} in detail while some of the SMs currently available in MTF are presented in the next section as examples of using the functionality described here to carry out the search in different ways. 

Another consequence of this conceptual impreciseness of \texttt{SM} is that a specific SM may use only a small subset of the functionality provided by the AM/SSM. 
%For instance, LK type SMs do not use the random sampling functions of \texttt{SSM} and conversely the stochastic SMs do not use the derivative functions required by the former.
This has two further implications. Firstly, the functionality set out in \texttt{AM} and \texttt{SSM} is not fixed but can change depending on the requirements of an SM, i.e. if a new SM is to be implemented that requires some functionality not present in the current specifications, the respective class can be extended to support it - as long as such an extension makes logical sense within the definition of that class. Secondly, it is not necessary for all combinations of AMs and SSMs to support all SMs. For instance a similarity measure does not need to be differentiable to be a valid AM as long as it is understood that it cannot be used with SMs that require derivatives.

In the broadest sense, the division of functionality between \texttt{AM} and \texttt{SSM} described next can be understood as \texttt{AM} being responsible for everything to do with the image $ I_t $, the sampled patch $ \mathbf{I_t}(\mathbf{x_t}) $ and the similarity $ f $ computed using it while \texttt{SSM} handles the actual \textit{points} $ \mathbf{x_t} $ at which the patch is sampled along with the warping function $ \mathbf{w} $ that defines it in terms of the reference points $ \mathbf{x_0} $ and state parameters $ \mathbf{p_t} $.

%This generic design means that once a specific search method has been written, any new appearance or state space model can be used with it as long as it implements all the functions defined in the respective base class in a meaningful way. Since there is no general way to specify a search method, this class has no functions except one to initialize the tracker with the object's initial location and then update it with each new frame. The other two classes, on the other hand, admit a much greater degree of generalization and thus have several common components that can be divided into following categories:
%\begin{enumerate}
%\item state variables
%\item methods to initialize and update state variables
%\item interfacing functions to combine the derivatives computed of both modules
%\end{enumerate}
%For example, \texttt{AppearanceModel} has following state variables:
%\begin{itemize}
%\item initial and current pixel values
%\item initial and current pixel gradients
%\item error vector and its norm
%\item gradient of the error vector and norm w.r.t. initial and current pixel values
%\end{itemize}

\subsection{\em{\texttt{AppearanceModel}}}
\label{sec_am}
This class can be divided into three main parts with each defined as a set of variables dependent on $ I_0 $ and $ I_t $ with a corresponding \texttt{initialize} and \texttt{update} function for each. The division is mainly conceptual and methods in different parts are free to interact with each other in practice. Table \ref{tab_am_func_specification} presents a brief specification of some important methods in \texttt{AM}.
\subsubsection{Image Operations}
This part, abstracted into a separate class called \texttt{ImageBase},  handles all pixel level operations on the image $ I $ like extracting the patch $ \mathbf{I}(\mathbf{x}) $ using sub pixel interpolation and computing its numerical gradient $ \dfrac{\partial \mathbf{I}}{\partial \mathbf{x}} $ and Hessian $ \dfrac{\partial^2 \mathbf{I}}{\partial \mathbf{x}^2} $. 

Though \texttt{AM} bears a composition or "has a" relationship with \texttt{ImageBase}, in practice the latter is actually implemented as a base class of the former to maintain simplicity of the interface and allow a specializing class to efficiently override functions in both classes. 
Moreover, having a separate class for pixel related operations means that AMs like Sum of Conditional Variance (SCV) and Zero mean Normalized Cross Correlation (ZNCC) that differ from SSD only in using a modified version of $ \mathbf{I_0} / \mathbf{I_t} $ (thus deriving from \texttt{SSDBase} in Fig. \ref{fig_class_diagram}), can implement the corresponding mapping entirely within the functions defined in \texttt{ImageBase}  and thus be combined easily with other similarity functions besides SSD.
\subsubsection{Similarity Functions}
This is the core of \texttt{AM} and handles the computation of the similarity measure $ f(\mathbf{I^*}, \mathbf{I^c}) $ and its derivatives $ \dfrac{\partial f}{\partial \mathbf{I}} $ and $ \dfrac{\partial^2 f}{\partial \mathbf{I}^2} $ w.r.t. both $ \mathbf{I^*} $ and $ \mathbf{I^c}$. It also provides interfacing functions to use inputs from \texttt{SSM} to compute the derivatives of $ f $ w.r.t. SSM parameters using the chain rule. As a notational convention, all interfacing functions, including those in \texttt{SSM}, are prefixed with \texttt{cmpt}.

Since several of the functions in this part involve common computations, there exist \textit{transitive dependency} relationships between them as depicted in Fig. \ref{fig_am_dependency} to avoid repeating these computations when multiple quantities are needed by the SM. What this means is that a function lower down in the dependency hierarchy may delegate part of its computations to any function higher up in the hierarchy so that the latter must be called \textit{before} calling the former if correct results are to be expected.
\begin{figure}[h]
\begin{center}
\includegraphics[width=3.4in]{am_dependency_2.png}
\caption{Dependency relationships between various functions in \texttt{AM}: an arrow pointing from A to B means that A depends on B. Color of a function box denotes its type - \textcolor{green}{green}: initializing; \textcolor{red}{red}: updating; \textcolor{blue}{blue}: interfacing and \textcolor{yellow}{yellow}: accessor function. Shape of a function box represents the part of \texttt{AM} it belongs to - rectangle: Image Operations; rounded rectangle: Similarity Functions; ellipse: Distance Feature. The numbers attached to some of the nodes refer Table \ref{tab_am_func_specification}}
\label{fig_am_dependency}
\end{center}
\end{figure}
\subsubsection{Distance Feature}
\label{sec_dist_feature}
This part is designed specifically to enable integration with the FLANN library \cite{Muja2009_flann} that is used by the NN based SM. It provides two main functions: 
\begin{enumerate}
\item A feature transform $ \mathbf{D}(\mathbf{I_1})  : \mathbb R^N\mapsto \mathbb R^K $   that maps the pixel values extracted from a patch $ \mathbf{I_1} $ into a feature vector that contains the results of all computations in $ f (\mathbf{I_1}, \mathbf{I_2}) $ that depend only on $ \mathbf{I_1} $. 
\item A highly optimized distance functor $f_D(\mathbf{I_1^D},\mathbf{I_2^D}) : \mathbb{R}^K \times \mathbb{R}^K \mapsto \mathbb{R}$ that computes a measure of the distance or dissimilarity between $ \mathbf{I_1} $ and $ \mathbf{I_2} $ (typically $ - f(\mathbf{I_1}, \mathbf{I_2}) $) given the distance features $ \mathbf{I_1^D} = \mathbf{D}(\mathbf{I_1}) $ and  $ \mathbf{I_2^D} = \mathbf{D}(\mathbf{I_2}) $ as inputs. 
\end{enumerate}
The main idea behind the design of these two components is to place as much computational load as possible on $ \mathbf{D} $ so that $f_D $ is as fast as possible with the premise that the former is called mostly during initialization when the sample dataset is to be built while the latter is called online to find the best matches for a candidate patch in the dataset. An optimal design may involve a trade off between the size $ K $ of the feature vector and the amount of computation performed in $f_D$.
For non symmetrical $ f $, i.e. where $ f(\mathbf{I_1}, \mathbf{I_2}) \neq f(\mathbf{I_2}, \mathbf{I_1})$ (e.g. CCRE), the first argument to  $f_D$ is to be interpreted as the $\mathbf{ I^*} $ and the second one as $ \mathbf{ I^c} $. A slight caveat is that, as the feature transform $ \mathbf{D} $ has no way of knowing which of these its input corresponds to, it must store computed results relevant to both  $\mathbf{ I^*} $ and  $\mathbf{ I^c} $.
\setlength{\floatsep}{0pt}
\setlength{\tabcolsep}{4pt}
\begin{table}
\scriptsize
\centering
\begin{threeparttable}
    \caption{Specifications for important methods in \texttt{AM}. IDs in first column refer Fig. \ref{fig_am_dependency}}
    \label{tab_am_func_specification}
    \begin{tabular}{|l|l|l|}	
     \hline
    \rowcolor{gray!50}
    ID & Inputs & Output/Variable updated \\    
    1 & $ \mathbf{x_t}$      & $\mathbf{I_t}(\mathbf{x_t})  $
    \\
    2 &  $ \mathbf{x_t}$      &  $ \nabla\mathbf{I_t} $
    \\
    3 & $ \mathbf{x_t}$      &  $ \nabla^2\mathbf{I_t} $
    \\
    4 & None      & $ f(\mathbf{I_0}, \mathbf{I_t}) $
    \\
    5 & None      &  $ \dfrac{\partial f(\mathbf{I_0}, \mathbf{I_t})}{\partial \mathbf{I_0}} $ 
    \\[2ex]
    6 & None      & $ \dfrac{\partial f(\mathbf{I_0}, \mathbf{I_t})}{\partial \mathbf{I_t}} $
    \\[2ex]
    7 &  $ \dfrac{\partial \mathbf{I_0}}{\partial \mathbf{p}} $      & $ \dfrac{\partial f(\mathbf{I_0}(\mathbf{p}), \mathbf{I_t})}{\partial \mathbf{p}} $  (Eq.  \ref{eq_jac_iclk})
    \\[2ex]
    8 &  $ \dfrac{\partial \mathbf{I_t}}{\partial \mathbf{p}} $      & $ \dfrac{\partial f(\mathbf{I_0}, \mathbf{I_t}(\mathbf{p}))}{\partial \mathbf{p}} $ (Eqs.  \ref{eq_jac_falk}, \ref{eq_jac_fclk})
    \\[2ex]
    9 & $ \dfrac{\partial \mathbf{I_0}}{\partial \mathbf{p}} $, $ \dfrac{\partial \mathbf{I_t}}{\partial \mathbf{p}} $      & $  \dfrac{\partial f(\mathbf{I_0}, \mathbf{I_t}(\mathbf{p}))}{\partial \mathbf{p}} - \dfrac{\partial f(\mathbf{I_0}(\mathbf{p}), \mathbf{I_t})}{\partial \mathbf{p}} $  (Eq.  \ref{eq_jac_esm})
    \\[2ex]
    10\tnote{1} & $ \dfrac{\partial \mathbf{I_0}}{\partial \mathbf{p}} $,$ \dfrac{\partial^2 \mathbf{I_0}}{\partial \mathbf{p}^2} $   & $ \dfrac{\partial^2 f(\mathbf{I_0}(\mathbf{p}), \mathbf{I_t})}{\partial \mathbf{p}^2} $
    \\[2ex]
    11 & $ \dfrac{\partial \mathbf{I_t}}{\partial \mathbf{p}} $,$ \dfrac{\partial^2 \mathbf{I_t}}{\partial \mathbf{p}^2} $      & $ \dfrac{\partial^2 f(\mathbf{I_0}, \mathbf{I_t}(\mathbf{p}))}{\partial \mathbf{p}^2} $
    \\[2ex]
    12 & $ \dfrac{\partial \mathbf{I_t}}{\partial \mathbf{p}} $,$ \dfrac{\partial^2 \mathbf{I_t}}{\partial \mathbf{p}^2} $      & $ \dfrac{\partial^2 f(\mathbf{I_t}, \mathbf{I_t}(\mathbf{p}))}{\partial \mathbf{p}^2} $  (Eqs.  \ref{eq_hess_iclk}, \ref{eq_hess_fclk})
    \\[2ex]
    13 & $ \dfrac{\partial \mathbf{I_0}}{\partial \mathbf{p}} $,$ \dfrac{\partial^2 \mathbf{I_0}}{\partial \mathbf{p}^2} $, $ \dfrac{\partial \mathbf{I_t}}{\partial \mathbf{p}} $,$ \dfrac{\partial^2 \mathbf{I_t}}{\partial \mathbf{p}^2} $      & $ \dfrac{\partial^2 f(\mathbf{I_0}(\mathbf{p}), \mathbf{I_t})}{\partial \mathbf{p}^2}  + \dfrac{\partial^2 f(\mathbf{I_0}, \mathbf{I_t}(\mathbf{p}))}{\partial \mathbf{p}^2}$
    \\[2ex]
     \hline
    \end{tabular}
    \begin{tablenotes}
    \item[1] Functions 10-13 have overloaded variants that omit the second term in Eq. \ref{eq_hess_basic}, as  in Eq. \ref{eq_hess_basic_gn}, and so do not require $ \dfrac{\partial^2 \mathbf{I}}{\partial \mathbf{p}^2} $ as input
    \end{tablenotes}
\end{threeparttable}
\end{table}

\subsection{\em{\texttt{StateSpaceModel}}}
\label{sec_ssm}
This class has a much simpler internal state than \texttt{AM} and can be described only three main variables at any time $ t $ : the sampled grid points $ \mathbf{x_t} $, the corresponding corners $ \mathbf{x^c_t} $ and the state parameters $ \mathbf{p_t} $. Though the definition of \texttt{SSM} can encompass any arbitrary warping function like the piecewise projective and B Spline transforms, all SSMs currently implemented are subsets of the planar projective transform and as such are derived from a single base class called \texttt{ProjectiveBase}.
The functions here can be divided into two distinct categories:
\subsubsection{Warping Functions}
\label{sec_ssm_warping_function}
This is the core of \texttt{SSM} and provides a function $ \mathbf{w} $ to transform a regularly spaced grid of points $ \mathbf{x_0} $ representing the target patch into a warped patch $ \mathbf{x_t} = \mathbf{w}(\mathbf{x_0}, \mathbf{p_t}) $ that captures the tracked object's motion in image space. 
%Its internal state at any given time $ t $ is defined by 3 main variables : the grid points $ \mathbf{x_t} $, the corresponding corners $\mathbf{x^c_t}  $ and the warp parameter vector $ \mathbf{p_t} $. 
It also provides functions to compute the derivatives of $ \mathbf{w} $ w.r.t. both $ \mathbf{x} $ and $ \mathbf{p} $ but, unlike AM, it is not designed to compute these separately, rather their computation is implicit in the interfacing functions that use chain rule to compute the image Jacobian and Hessian. This design decision was made for reasons of efficiency since $ \dfrac{\partial\mathbf{w}}{\partial \mathbf{p}} $ and $\dfrac{\partial\mathbf{w}}{\partial \mathbf{x}}$ are large and often very sparse tensors and computing these separately not only wastes a lot of memory but is also very computationally inefficient. It provides four ways to update the internal state: incrementally using additive (\texttt{additiveUpdate}) or compositional (\texttt{compositionalUpdate}) formulations, or outright by providing either the state vector (\texttt{setState}) or the corresponding corners (\texttt{setCorners}) that define the current location of the patch.
There are no complex dependencies in \texttt{SSM} - the correct performance of the interfacing functions and accessors depends entirely on one of the update functions being called every iteration. Table \ref{tab_ssm_func_specification} lists the functionality of some important methods in this part of \texttt{SSM}.
\setlength{\tabcolsep}{4pt}
\begin{table}
\centering
    \caption{Specifications for important methods in \texttt{SSM}.}
    \label{tab_ssm_func_specification}   
    \begin{tabular}{|p{3.0cm}|l|p{3.2cm}|}	
     \hline
    \rowcolor{gray!50}
    Function & Inputs & Output/Result \\
     \scriptsize{\texttt{compositionalUpdate}} & \scriptsize{$ \Delta\mathbf{p} $}      & \scriptsize{$ \mathbf{p_t}=\mathbf{p}'\ |\ \mathbf{w}(\mathbf{x}, \mathbf{p}')= \mathbf{w}(\mathbf{w}(\mathbf{x}, \Delta \mathbf{p}), \mathbf{p_t})  $}
     \\ 
    \scriptsize{\texttt{additiveUpdate}} &  \scriptsize{$ \Delta\mathbf{p} $}      &  \scriptsize{$ \mathbf{p_t} = \mathbf{p_t} +  \Delta\mathbf{p} $}
    \\ 
    \scriptsize{\texttt{invertState}} & \scriptsize{$ \mathbf{p} $ }     &  \scriptsize{$\mathbf{p}'\ |\ \mathbf{w}(\mathbf{w}(\mathbf{x}, \mathbf{p}), \mathbf{p'}) = \mathbf{x} $ }
    \\ 
    \scriptsize{\texttt{cmptPixJacobian}} & \scriptsize{$ \nabla\mathbf{I_t} $}      & \scriptsize{$ \left.\dfrac{\partial\mathbf{I_t}}{\partial\mathbf{p}} \right\rvert_{\mathbf{p}=\mathbf{p_t}}$} (Eq. \ref{eq_jac_falk})
    \\[2ex]
    \scriptsize{\texttt{cmptWarpedPixJacobian}} & \scriptsize{$ \nabla\mathbf{I_t}$}      &  \scriptsize{$  \left.
    \dfrac{\partial\mathbf{I_t}(\mathbf{w})}{\partial \mathbf{p}}
    \right\rvert_{\mathbf{p}=\mathbf{p_0}}
    $} (Eqs. \ref{eq_jac_fclk}, \ref{eq_jac_warped_grad})
    \\[2ex]
    \scriptsize{\texttt{cmptApproxPixJacobian}} & \scriptsize{$ \nabla\mathbf{I_0} $}      & \scriptsize{
%    $ \dfrac{\partial \mathbf{I_0}}{\partial \mathbf{x}}
%     \dfrac{\partial \mathbf{w}}{\partial \mathbf{x}}^{-1}
%     \dfrac{\partial \mathbf{w}}{\partial \mathbf{p}} $
$ \dfrac{\partial \mathbf{I_t}}{\partial \mathbf{p_t}}$ (approx)
     } (Eq. \ref{eq_jac_ialk}, \ref{eq_ialk_assumption})
     \\[2ex]
    \scriptsize{\texttt{cmptPixHessian}} &   \scriptsize{$ \nabla\mathbf{I_t}, \nabla^2\mathbf{I_t}  $}      & $ 
    \left.
    \dfrac{\partial^2 \mathbf{\mathbf{I_t}}}{\partial \mathbf{p}^2}
    \right\rvert_{\mathbf{p}=\mathbf{p_t}}
    $  (Eq. \ref{eq_pix_hess_falk})
    \\[2ex]
    \scriptsize{\texttt{cmptWarpedPixHessian}} &  \scriptsize{$\nabla\mathbf{I_t}, \nabla^2\mathbf{I_t} $}      & \scriptsize{$
    \left.
    \dfrac{\partial^2 \mathbf{\mathbf{I_t}(\mathbf{w})}}{\partial \mathbf{p}^2}
    \right\rvert_{\mathbf{p}=\mathbf{p_0}}
    $(Eqs. \ref{eq_pix_hess_fclk}, \ref{eq_warped_pix_hess})}
    \\[2ex]
     \scriptsize{\texttt{cmptApproxPixHessian}} & \scriptsize{$\nabla\mathbf{I_0}, \nabla^2\mathbf{I_0} $}      & \scriptsize{$  \dfrac{\partial^2 \mathbf{\mathbf{I_t}}}{\partial \mathbf{p_t}^2} $ (approx) (Eq. \ref{eq_ialk_pix_hess}) }
     \\[2ex]
     \hline
    \end{tabular}
\end{table}
\subsubsection{Stochastic Sampler}
\label{sec_ssm_stochastic_sampler}
This part is provided to support SMs based on random search and provides following functionality to this end:
\begin{enumerate}
\item generate small random incremental updates to $ \mathbf{p} $ (\texttt{generatePerturbation}) by drawing these from a zero mean normal distribution with either user provided or heuristically estimated (\texttt{estimateStateSigma}) variance. 
\item generate stochastic state samples using the given dynamic or state transition model - currently random walk (\texttt{additiveRandomWalk}) and first order auto regression (\texttt{additiveAutoRegression1}) are supported. There are also \texttt{compositional} variants to these functions.
\item estimate the mean of a set of samples (\texttt{estimateMeanOfSamples})
\end{enumerate}

